#include <stdio.h>
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "time.h"

#define ON      1
#define OFF     0
#define LED_ESP 2

void delay(int msec)
{ 
    int start_time;
    for (start_time = clock(); clock() < start_time + msec;);
}

void app_main(void)
{
    gpio_reset_pin(LED_ESP);
    gpio_set_direction(LED_ESP, GPIO_MODE_OUTPUT);
    
    while(1) 
    {
        printf("LED OFF with my delay.\n");
        gpio_set_level(LED_ESP, OFF);
        delay(200);

        printf("LED ON with my delay.\n");
        gpio_set_level(LED_ESP, ON);
        delay(200);
    }
}